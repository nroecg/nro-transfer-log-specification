Current Status
==============

This has been approved by the ECG. This specification has been implemented by all RIRs.

Changes
=======

4.0.0
-----

- Changes to the names of the transfer types.
- Version bump was necessary as there were incompatibility issues with 3.0 between some RIRs.

3.0.0
-----

- Initial JSON specification.

1.0.0 - 2.0.0
-------------

There are no versions 1 or 2 of this specification. The idea for this specification was originally a clarification for the
Extended Statistics files, which have these verion numbers. To avoid confusion, this specification started at 3.0.0.
